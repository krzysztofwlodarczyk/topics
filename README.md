## Tematy

1. Wolne zapytania SQL - jak je detekowac i co mozna zrobic zeby je przyspieszyc
2. REST API - jakie frameworki, jakie metody HTTP, czym rozni PUT od PATCH
3. Generatory - co to i wlasciwosci oraz yield i next
4. Bazy relacyjne i NoSQL
5. Branchowanie w GIT - jakie podejscie
6. Co uzywasz przy codziennej pracy
7. classmethod co to 
8. Kompilacja python
9. Docker staging
10. Dekoratory
11. Deskryptor
12. Asynchronize
13. Multiprocessing, multithreading
14. Access modifiers
15. nonlocal
16. Jakie nowosci z pythona3 uzywasz/lubisz
17. Nested function
18. Python Closures
19. Struktury danych w pythonie
20. Context Manager
21. GIL
22. Magic methods
23. Drzewo w pythonie sie zdaza
24. Unitesty
25. __init__
26. Definicja self
27. Process, thread, czym sie rozni jak sie ma do tego GIL
28. Functools itertools
29. Prosty web api in django
images z link = "https://api.thecatapi.com/v1/images/search?limit=3"
30. Context manager
31. classmethod
32. index bazdy danych, views, temporialy tables (bulldog)
33. Wlasne rest api
34. Kompilacja python - gc weakref - roznice
35. Docker staging
36. Closure in python
37. Struktury danych w python
38. Next yield
39. PUT
40. Diff between python 2.7 and 3 - what you use from python3
41. First-Class Objects - This means that functions can be passed around and used as arguments
42. iterators
43. is roznice ==
44. sinlgeton - Borg wariant singletona - singleton to zly pattern ogolnie i ze trzeba unikac
45. classmethod
46. __enter__ - wlasny context manager
47. program do zgodnosci z pep8
48. proces a thread
49. async
50. Python Design Patterns - Singleton



TODO:
1. Kubernetes plus terraform
2. Rest API
3. Cloudy
4. Pytest
5. Django
6. Flask
7. Pandas?
8. Numpy?


